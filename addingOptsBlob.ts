// Select the element with class "js-blob-header-filepath" which contains the filename
const fileNameElement = document.querySelector('.js-blob-header-filepath');

if (fileNameElement) {
  // Get the text content of the filename element
  const fileName = fileNameElement.textContent;

  // Check if the file extension is ".acd" or ".l5k"
  if (fileName?.endsWith('.acd') || fileName?.endsWith('.l5k')) {
    // Create a button element
    const button = document.createElement('button');
    button.textContent = 'Options';

    // Add a click event handler to the button
    button.addEventListener('click', () => {
      // Add your button's functionality here
      alert('Options clicked for ' + fileName);
    });

    // Find the "file-actions" element
    const fileActionsElement = document.querySelector('.file-actions');

    if (fileActionsElement) {
      // Insert the new button as the first child of the "file-actions" element
      fileActionsElement.insertBefore(button, fileActionsElement.firstChild);
    }
  }
}
