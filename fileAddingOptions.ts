const tableHead = document.querySelector('thead');

// Find the existing <th> element with the ID "name"
const existingNameHeader = tableHead?.querySelector('th#name');

if (existingNameHeader) {
  // Create a new <th> element for the "Options" header
  const optionsHeader = document.createElement('th');
  optionsHeader.textContent = 'Options';

  // Insert the new "Options" header <th> element after the existing "Name" header
  existingNameHeader.parentNode?.insertBefore(optionsHeader, existingNameHeader.nextSibling);
}

// Select all <tr> elements with the class "tree-item" within a single table
const treeItemRows = document.querySelectorAll('table tr.tree-item');

// Loop through each <tr> element within the table
treeItemRows.forEach((treeItem) => {
  // Check if the current row contains a link to a file with either ".acd" or ".l5k" extension
  const fileNameLink = treeItem.querySelector('a.tree-item-link');
  if (fileNameLink) {
    const fileName = fileNameLink.getAttribute('title');
    if (fileName?.endsWith('.acd') || fileName?.endsWith('.l5k')) {
      // Create a button element
      const button = document.createElement('button');
      button.textContent = 'My Button';

      // Add a click event handler to the button
      button.addEventListener('click', () => {
        // Add your button's functionality here
        alert('Button clicked for ' + fileName);
      });

      // Create a new <td> element for the button
      const buttonCell = document.createElement('td');
      buttonCell.appendChild(button);

      // Insert the button <td> element after the <td> containing the <a> tag
      const fileNameCell = treeItem.querySelector('td.tree-item-file-name');
      fileNameCell?.parentNode?.insertBefore(buttonCell, fileNameCell.nextSibling);
    } else {
      // Create a <p> element for the "No Options" message
      const noOptionsMessage = document.createElement('p');
      noOptionsMessage.textContent = 'No Options';

      // Create a new <td> element for the message
      const messageCell = document.createElement('td');
      messageCell.appendChild(noOptionsMessage);

      // Insert the message <td> element after the <td> containing the <a> tag
      const fileNameCell = treeItem.querySelector('td.tree-item-file-name');
      fileNameCell?.parentNode?.insertBefore(messageCell, fileNameCell.nextSibling);
    }
  }
});
